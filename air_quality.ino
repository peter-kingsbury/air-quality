#include <LiquidCrystal_I2C.h>

int pin2 = 3;
int pin1 = 2;
unsigned long duration1;
unsigned long duration2;

LiquidCrystal_I2C lcd(0x27,20,4);

unsigned long starttime;
unsigned long sampletime_ms = 3000;
unsigned long lowpulseoccupancy1 = 0;
unsigned long lowpulseoccupancy2 = 0;
float ratio1 = 0;
float ratio2 = 0;
float concentration1 = 0;
float concentration2 = 0;
int wLed = 8;
int gLed = 7;
int yLed = 6;
int rLed = 5;
int bLed = 4;
char buf[16];
char fmt[8];

void lcd_clear_screen() {
  lcd.setCursor(0, 0);
  lcd.print("                 ");
  lcd.setCursor(0, 1);
  lcd.print("                 ");
}

void lcd_write(int row, int col, char* text) {
  lcd.setCursor(row, col);
  lcd.print(text);
}

void setup() {
  Serial.begin(9600);

  lcd.init();
  lcd.backlight();
  lcd_clear_screen();
  
  pinMode(wLed,OUTPUT);
  pinMode(gLed,OUTPUT);
  pinMode(yLed,OUTPUT);
  pinMode(rLed,OUTPUT);
  pinMode(bLed,OUTPUT);
  starttime = millis();//get the current time;
}

void loop() {
  // duration1 is PM2.5
  // duration2 is PM10
  duration1 = pulseIn(pin1, LOW);
  duration2 = pulseIn(pin2, LOW);
  lowpulseoccupancy1 = lowpulseoccupancy1+duration1;
  lowpulseoccupancy2 = lowpulseoccupancy2+duration2;

  // if the sample time == 30s
  if ((millis()-starttime) > sampletime_ms) {
    // Integer percentage 0=>100
    ratio1 = lowpulseoccupancy1/(sampletime_ms*10.0);  

    // using spec sheet curve
    concentration1 = 1.1*pow(ratio1,3)-3.8*pow(ratio1,2)+520*ratio1+0.62; 

    // Integer percentage 0=>100
    ratio2 = lowpulseoccupancy2/(sampletime_ms*10.0);
    concentration2 = 1.1*pow(ratio2,3)-3.8*pow(ratio2,2)+520*ratio2+0.62;

    Serial.print("concentration1 = ");
    Serial.print(concentration1);
    Serial.print(" pcs/0.01cf  -  ");

    Serial.print("concentration2 = ");
    Serial.print(concentration2);
    Serial.print(" pcs/0.01cf\n");

    dtostrf(concentration2, 8, 3, fmt);

    sprintf(buf, "PM10 %s", fmt);
    lcd_write(0, 0, buf);
    
    
    if (concentration1 < 1000) {
      // CLEAN
      lcd_write(0, 1, "     Clean      ");
      digitalWrite(wLed, HIGH);
      digitalWrite(gLed, LOW);
      digitalWrite(yLed, LOW);
      digitalWrite(rLed, LOW);
      digitalWrite(bLed, LOW);
    } else if (concentration1 > 1000 && concentration1 < 10000) {
      // GOOD
      lcd_write(0, 1, "      Good      ");
      digitalWrite(wLed, LOW);
      digitalWrite(gLed, HIGH);
      digitalWrite(yLed, LOW);
      digitalWrite(rLed, LOW);
      digitalWrite(bLed, LOW);
    } else if (concentration1 > 10000 && concentration1 < 20000) {
      // ACCEPTABLE
      lcd_write(0, 1, "   Acceptable   ");
     digitalWrite(wLed, LOW);
     digitalWrite(gLed, LOW);
     digitalWrite(yLed, HIGH);
     digitalWrite(rLed, LOW);
     digitalWrite(bLed, LOW);
    } else if (concentration1 > 20000 && concentration1 < 50000) {
      // HEAVY
      lcd_write(0, 1, "     Heavy      ");
      digitalWrite(wLed, LOW);
      digitalWrite(gLed, LOW);
      digitalWrite(yLed, LOW);
      digitalWrite(rLed, HIGH);
      digitalWrite(bLed, LOW);
    } else if (concentration1 > 50000 ) {
      // HAZARD
      lcd_write(0, 1, "     Hazard     ");
      digitalWrite(wLed, LOW);
      digitalWrite(gLed, LOW);
      digitalWrite(yLed, LOW);
      digitalWrite(rLed, LOW);
      digitalWrite(bLed, HIGH);
    } 

    lowpulseoccupancy1 = 0;
    lowpulseoccupancy2 = 0;
    starttime = millis();
  }
}
